#!/usr/bin/python

#IMPORTS
import sys
import time
import mysql.connector
from mysql.connector import errorcode
import random
import datetime
from os import system, name

#VARIABLEN
connection = None
datenbankname = "pseudoTemperaturMessdaten"
user = "test"
password = "test"
host = "127.0.0.1"
input = None
input2 = None
input3 = None



#METHODEN

def messen(temperatur):
    date = time.strftime('%Y-%m-%d %H:%M:%S')
    temperatur = temperatur + random.uniform(-1, 1)
    print("Datum: {0} , Temperatur: {1:0.2f}".format(date,temperatur))
    speichern("data", (date, "{0:0.2f}".format(temperatur)))
    return temperatur


def speichern(table, data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO "+table+" (datetime, value) VALUES (%s, %s)", data)
    connection.commit()
    cursor.close()

def select(sql):
    cursor = connection.cursor()
    cursor.execute(sql)
    result = cursor.fetchall()
    for x in result:
        print(x)
    cursor.close()
    raw_input()


def createDatabase():
    cursor = connection.cursor()
    cursor.execute("CREATE DATABASE {0}".format(datenbankname))
    cursor.close()
    print("Datenbank erstellt")


def createTable():
    cursor = connection.cursor()
    cursor.execute(createStatement)
    cursor.close()
    print("Tabelle erstellt")

def clear():
    if name == 'nt':
        _ = system('cls')
    else:
        _ = system('clear')


#PROGRAMM



try:
    print("Verbindung zu Datenbank "+datenbankname+" wird aufgebaut")
    connection = mysql.connector.connect(user=user, password=password, host=host, database=datenbankname)
    print("Verbindung aufgebaut")
    
    createTable()


except mysql.connector.Error as e:
    if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("LOGINDATEN SIND FALSCH")
    elif e.errno == errorcode.ER_BAD_DB_ERROR:
        print("DATENBANK EXISTIERT NICHT")
        print("Datenbank wird versucht zu erstellen!")
        connection = mysql.connector.connect(user=user, password=password, host=host)
        createDatabase()
        connection.database = datenbankname
        print("Verbindung hergestellt")
        createTable()
    else:
        print(str(e))
        sys.exit(1)
except:
    print("ERROR: "), sys.exc_info()[0]


while input != "b":
    clear()
    print("Programm beenden: b")
    print("Input: i")
    print("Output: o")
    input = raw_input()
    if input == "b":
        print("Programm beendet")
        sys.exit(1)
    elif input == "i":
        while input2 != "b":
            clear()
            print("Programm beenden: b")
            print("Nur Datenwert insertieren: n")
            print("Daten und Uhrzeit insertieren: d")
            input2 = raw_input()
            if input2 == "b":
                print("Input beendet")
            elif input2 == "n":
                print("Welcher Temperaturwert soll insertiert werden?")
                print("Punkt als Dezimaltrennzeichen!")
                temperatur2 = raw_input()
                date = time.strftime('%Y-%m-%d %H:%M:%S')
                speichern("data", (date, "{0:0.2f}".format(float(temperatur2))))
            elif input2 == "d":
                print("Welcher Temperaturwert soll insertiert werden?")
                print("Punkt als Dezimaltrennzeichen!")
                temperatur2 = raw_input()
                print("Jahr?")
                year = int(raw_input())
                print("Monat?")
                month = int(raw_input())
                print("Tag?")
                day = int(raw_input())
                print("Stunde?")
                hour = int(raw_input())
                print("Minute?")
                minute = int(raw_input())
                print("Sekunde?")
                sekunde = int(raw_input())
                t = (year, month, day, hour, minute, sekunde, 0, 0, 0)
                date2 = time.mktime(t)
                date = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(date2))
                speichern("data", (date, "{0:0.2f}".format(float(temperatur2))))
    elif input == "o" :
        while input3 != "b":
            clear()
            print("Programm beenden: b")
            print("Selektieren des Durchschnitt aller Messdaten: a")
            print("Die letzten 6 Werte selektieren: s")
            print("Select der Hoechsttemperatur: h")
            print("Select der niedrigsten Temperatur: n")
            input3 = raw_input()
            if input3 == "b":
                print("Input beendet")
            elif input3 == "a":
                select("select avg(value) from data")
            elif input3 == "s":
                select("select * from data order by datetime desc limit 6")
            elif input3 == "h":
                select("select max(value) from data")
            elif input3 == "n":
                select("select min(value) from data")

    else:
        print("Kein Richtiger Input")



