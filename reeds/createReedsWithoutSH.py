#!/usr/bin/env python3

#VARIABLEN

FEED_URI="https://www.oeamtc.at/feeds/verkehr/tirol"

#RSS-FEED

import feedparser
from dateutil import parser

d = feedparser.parse(FEED_URI)

n = len(d)

for i in range(n):
    
    for j in range(0, n-i-1):
        
        dt1 = parser.parse(d.entries[j].published)
        dt2 = parser.parse(d.entries[j+1].published)
        
        if dt1 > dt2:
            d.entries[j], d.entries[j+1] = d.entries[j+1], d.entries[j]

#GET ASFINAG WEBCAMS

import os

print("trying to get zirl picture")
os.system("wget -q 'https://webcams2.asfinag.at/webcamviewer/SingleScreenServlet//showcam?user=webcamstartseite&camname=VK_A12_087,345-V0-FL-I&ssid=150219020196582' -O  "+PICS_PATH+"zirl.jpg")
print("zirl picture received")
print("trying to get voels picture")
os.system("wget -q 'https://webcams2.asfinag.at/webcamviewer/SingleScreenServlet//showcam?user=webcamstartseite&wcsid=A012_086,471_1_00012836&ssid=638970a6-9216-49f1-beff-f0ef264cc41d' -O  "+PICS_PATH+"voels.jpg")
print("voels picture received")
print("trying to get inzing picture")
os.system("wget -q 'https://webcams2.asfinag.at/webcamviewer/SingleScreenServlet//showcam?user=webcamstartseite&camname=VK_A12_082,178~F1-I&ssid=150219034224229&time=0.6186638975742357' -O  "+PICS_PATH+"inzing.jpg")
print("inzing picture received")
print("trying to get kematen picture")
os.system("wget -q 'https://webcams2.asfinag.at/webcamviewer/SingleScreenServlet//showcam?user=webcamstartseite&wcsid=A012_084,347_2_00009522&ssid=bb511a0f-ea59-4dbe-b87b-a7695b689536' -O  "+PICS_PATH+"kematen.jpg")
print("kematen picture received")
print("trying to get ibkwest picture")
os.system("wget -q 'https://webcams2.asfinag.at/webcamviewer/SingleScreenServlet//showcam?user=webcamstartseite&wcsid=A012_078,972_1_00012832&ssid=797b0934-685d-4aa2-9c8c-8b91763815c3' -O  "+PICS_PATH+"ibkwest.jpg")
print("ibkwest picture received")


#GENERATE PICTURE

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib.gridspec as gridspec
import time

import configparser
import os

os.chdir(str(os.path.dirname(os.path.abspath(__file__))))
config = configparser.ConfigParser()
config.read('config.ini')

fig = plt.figure(figsize=(12.8, 10.24))
gs = gridspec.GridSpec(3, 5)
gs.update(left=0.01, right=0.99, wspace=0.01, hspace=0.05)

ax0 = plt.subplot(gs[0, 0])
ax1 = plt.subplot(gs[0, 1])
ax2 = plt.subplot(gs[0, 2])
ax3 = plt.subplot(gs[0, 3])
ax4 = plt.subplot(gs[0, 4])

subplots = [ax0,ax1,ax2,ax3,ax4]

for i in range(0, 5):
    subplots[i].set_facecolor('black')
    subplots[i].set_xticks([])
    subplots[i].set_yticks([])
    subplots[i].set_xticklabels([])
    subplots[i].set_yticklabels([])


pathwebcampics = '/tmp/'
im_voels = plt.imread(pathwebcampics + 'voels.jpg')
im_zirl = plt.imread(pathwebcampics + 'zirl.jpg')
im_inzing = plt.imread(pathwebcampics + 'inzing.jpg')
im_ibkwest = plt.imread(pathwebcampics + 'ibkwest.jpg')
im_kematen = plt.imread(pathwebcampics + 'kematen.jpg')

ax0.imshow(im_zirl, aspect='auto')
ax1.imshow(im_voels, aspect='auto')
ax2.imshow(im_kematen, aspect='auto')
ax3.imshow(im_inzing, aspect='auto')
ax4.imshow(im_ibkwest, aspect='auto')

ax0.text(25, 75, "ZIRL", fontsize=20, color='red', alpha=1)
ax1.text(25, 40, "INZING", fontsize=20, color='red', alpha=1)
ax2.text(25, 75, "KEMATEN", fontsize=20, color='red', alpha=1)
ax3.text(25, 40, "VÖLS", fontsize=20, color='red', alpha=1)
ax4.text(25, 40, "IBK-WEST", fontsize=20, color='red', alpha=1)

fig.text(0.27, 0.93, "VERKEHRSSITUATION TIROL", fontsize=30, color='white', alpha=1)
createDate = time.strftime('%d-%m-%Y %H:%M:%S')
fig.text(0.01, 0.98, createDate, fontsize=10, color='white', alpha=1)


print("---")
for i in range(1,6):
    fig.text(0.015, (0.6-0.115*(i-1)), d.entries[len(d)-i].title, fontsize=20, color='darkorange', alpha=1, horizontalalignment='left', verticalalignment='top')
    fig.text(0.035, (0.57-0.115*(i-1)), d.entries[len(d)-i].description, fontsize=15, color='white', alpha=1, wrap=True, horizontalalignment='left', verticalalignment='top')
    fig.text(0.015, (0.605-0.115*(i-1)), "Veröffentlicht: " + d.entries[len(d)-i].published, fontsize=7, color='white', alpha=1)
    print(d.entries[len(d)-i].title)
    print(d.entries[len(d)-i].description)
    print(d.entries[len(d)-i].published)
    print("---")

fig.savefig(config.get('main', 'PATH_GRAPH_OUTPUT') + os.sep + 'reeds.png', facecolor='black')   # save the figure to file
plt.close(fig)
