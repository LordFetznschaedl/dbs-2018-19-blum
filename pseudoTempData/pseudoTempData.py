#!/usr/bin/python

#IMPORTS
import sys
import time
import mysql.connector
from mysql.connector import errorcode
import random

#VARIABLEN
connection = None
datenbankname = "pseudoTemperaturMessdaten"
user = "test"
password = "test"
host = "127.0.0.1"
temperatur = random.uniform(0,30)

createStatement = "CREATE TABLE IF NOT EXISTS data(datetime DATETIME NOT NULL,value FLOAT(4,1) SIGNED NOT NULL)"

#METHODEN

def messen(temperatur):
    date = time.strftime('%Y-%m-%d %H:%M:%S')
    temperatur = temperatur + random.uniform(-1, 1)
    print("Datum: {0} , Temperatur: {1:0.2f}".format(date,temperatur))
    speichern("data", (date, "{0:0.2f}".format(temperatur)))
    return temperatur


def speichern(table, data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO "+table+" (datetime, value) VALUES (%s, %s)", data)
    connection.commit()
    cursor.close()


def createDatabase():
    cursor = connection.cursor()
    cursor.execute("CREATE DATABASE {0}".format(datenbankname))
    cursor.close()
    print("Datenbank erstellt")


def createTable():
    cursor = connection.cursor()
    cursor.execute(createStatement)
    cursor.close()
    print("Tabelle erstellt")


#PROGRAMM
if len(sys.argv) < 2:
    print("ZU WENIGE PARAMETER")
    sys.exit(1)
elif len(sys.argv) > 2:
    print("ZU VIELE PARAMETER")
    sys.exit(1)


try:
    print("Verbindung zu Datenbank "+datenbankname+" wird aufgebaut")
    connection = mysql.connector.connect(user=user, password=password, host=host, database=datenbankname)
    print("Verbindung aufgebaut")
    
    createTable()


except mysql.connector.Error as e:
    if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("LOGINDATEN SIND FALSCH")
    elif e.errno == errorcode.ER_BAD_DB_ERROR:
        print("DATENBANK EXISTIERT NICHT")
        print("Datenbank wird versucht zu erstellen!")
        connection = mysql.connector.connect(user=user, password=password, host=host)
        createDatabase()
        connection.database = datenbankname
        print("Verbindung hergestellt")
        createTable()
    else:
        print(str(e))
        sys.exit(1)
except:
    print("ERROR: "), sys.exc_info()[0]


while(True):
    temperatur = messen(temperatur)
    print(temperatur)
    time.sleep(3600 / int(sys.argv[1]))


