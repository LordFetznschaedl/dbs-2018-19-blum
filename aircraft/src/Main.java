
import java.awt.Desktop;
import java.io.*;
import java.sql.*;
import java.util.GregorianCalendar;



public class Main
{
	public static String user = "test";
	public static String pw = "test";
	public static String sql;
	public static Connection c;
	public static Statement stmt;
	public static String dateiname=null;
	public static BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

	public static void main( String args[] ){
		try {
			prepareDatabase();

			
			datenMonatAnzeigen();
					



		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	public static void datenMonatAnzeigen() throws IOException, SQLException {
		File file = new File("data.txt");
		file.delete();

		FileWriter fw = new FileWriter("data.txt");
		BufferedWriter bw = new BufferedWriter(fw);

		bw.write("["+getAircraftsFromNow(12)+"");
		for(int i=11;i>=1;i--) {
			bw.write(","+getAircraftsFromNow(i));
		}
		bw.write("]");

		bw.close();
		fw.close();

		file=new File("Chart.html");
		Desktop desk = Desktop.getDesktop();
		desk.open(file);
	}
	public static String getAircraftsFromMax(int month) throws IOException, SQLException {
		sql="SELECT MAX(seentime) FROM DUMP1090DATA;";
		stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		rs.next();
		long max = Long.parseLong(rs.getString("MAX(seentime)"));

		Date date=new Date(max*1000);

		GregorianCalendar untergrenze=new GregorianCalendar();
		untergrenze.setTime(date);
		untergrenze.add(GregorianCalendar.MONTH, -month);

		GregorianCalendar obergrenze=new GregorianCalendar();
		obergrenze.setTime(date);
		obergrenze.add(GregorianCalendar.MONTH, -month+1);

		sql="SELECT COUNT(seentime) FROM DUMP1090DATA WHERE "
				+ "seentime>"+untergrenze.getTimeInMillis()/1000+" AND "
				+ "seentime<"+obergrenze.getTimeInMillis()/1000+";";

		stmt = c.createStatement();
		rs = stmt.executeQuery(sql);
		rs.next();

		String count = rs.getString("COUNT(seentime)");

		rs.close();
		stmt.close();

		return count;
	}
	
	public static String getAircraftsFromNow(int month) throws IOException, SQLException {
		stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		GregorianCalendar untergrenze=new GregorianCalendar();
		untergrenze.add(GregorianCalendar.MONTH, -month);

		GregorianCalendar obergrenze=new GregorianCalendar();
		obergrenze.add(GregorianCalendar.MONTH, -month+1);

		sql="SELECT COUNT(DISTINCT seentime) FROM DUMP1090DATA WHERE "
				+ "seentime>"+untergrenze.getTimeInMillis()/1000+" AND "
				+ "seentime<"+obergrenze.getTimeInMillis()/1000+";";

		stmt = c.createStatement();
		rs = stmt.executeQuery(sql);
		rs.next();

		String count = rs.getString("COUNT(DISTINCT seentime)");

		rs.close();
		stmt.close();

		return count;
	}

	
	

	public static void prepareDatabase() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		/*
		System.out.println("Bitte MySQL-User angeben: ");
		user = console.readLine();

		System.out.println("Bitte MySQL-Passwort eingeben: ");
		pw = console.readLine();
		 */
		c = DriverManager.getConnection("jdbc:mysql://localhost:3306?useSSL=false", user, pw);

		stmt = c.createStatement();
		sql="USE aircraft;";
		stmt.executeUpdate(sql);
		System.out.println("Database in use");
		stmt.close();

		System.out.println("");
	}
}
