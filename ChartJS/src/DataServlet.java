

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DataServlet
 */
@WebServlet("/DataServlet")
public class DataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static Connection c = null;
	public static Statement stmt=null;
	public static ResultSet rs = null;
	public static ArrayList<Double> data = new ArrayList<Double>();
	public static ArrayList<String> label = new ArrayList<String>();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DataServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
 		
		try {

			Class.forName("com.mysql.jdbc.Driver").newInstance();

			
			c = DriverManager.getConnection("jdbc:mysql://localhost?useSSL=false","test","test");
			stmt= c.createStatement();

			stmt.execute("use pseudoTemperaturMessdaten;");
			System.out.println("connected to database");

			rs = stmt.executeQuery("select * from data order by datetime desc limit 6;");
			
			data.clear();
			label.clear();
			
			while(rs.next())
			{
				
				data.add(rs.getDouble("value"));
				System.out.println(rs.getDouble("value"));
				label.add(rs.getString("datetime"));
				System.out.println(rs.getString("datetime"));
			}
			stmt.close();
			rs.close();

			Collections.reverse(data);
			Collections.reverse(label);
			
			request.setAttribute("data1", (Double)data.get(0));
			request.setAttribute("label1", (String)label.get(0));
			request.setAttribute("data2", (Double)data.get(1));
			request.setAttribute("label2", (String)label.get(1));
			request.setAttribute("data3", (Double)data.get(2));
			request.setAttribute("label3", (String)label.get(2));
			request.setAttribute("data4", (Double)data.get(3));
			request.setAttribute("label4", (String)label.get(3));
			request.setAttribute("data5", (Double)data.get(4));
			request.setAttribute("label5", (String)label.get(4));
			request.setAttribute("data6", (Double)data.get(5));
			request.setAttribute("label6", (String)label.get(5));
			
			
			

		} 
		catch(Exception e)
		{
			System.out.println("Exception: "+ e.getMessage());
			e.printStackTrace();
		}
		
		RequestDispatcher d = request.getRequestDispatcher("Index.jsp");
		d.forward(request, response);
	}

}
