<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">

	<title>Line Chart</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

	<style>
		canvas {
			-moz-user-select: none;
			-webkit-user-select: none;
			-ms-user-select: none;
		}
	</style>
</head>
<body>

	<%

		
		
	%>
	<div style="width:100%">
        <canvas id="canvas"></canvas>
     
    </div>

    <br />
    <br />

    <script>

      	var data = [];
      	var label = [];

   

        var config = {
            type: 'line',
            data: {
                labels: label,
                datasets: [{
                    label: 'Temperatur [\xB0C]',
                    lineTension: 0,
                    fill: false,
                    showLines: true,
                    backgroundColor: 'rgb(255, 100, 50)',
                    borderColor: 'rgb(255, 100, 50)',
                    data: data,
                   

                }]
            },
            options: {

                responsive: true,
                showLines: true,
                title: {
                    display: true,
                    text: 'Messdaten'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {

                    xAxes: [{
                        display: true,
                        distribution: 'series',
                        scaleLabel: {
                            display: true,
                            labelString: 'Messzeitpunkte'

                        },


                    }],

                    yAxes: [{

                        display: true,
                        scaleLabel: {
                            display: true,

                            labelString: 'Messwerte Temperatur [\xB0C]'
                        },
                    }]
                }
            }
        };

        window.onload = function () {
            
        	data.push(<%request.getAttribute("data1");%>);
        	label.push(<%request.getAttribute("label1");%>);
        	data.push(<%request.getAttribute("data2");%>);
        	label.push(<%request.getAttribute("label2");%>);
        	data.push(<%request.getAttribute("data3");%>);
        	label.push(<%request.getAttribute("label3");%>);
        	data.push(<%request.getAttribute("data4");%>);
        	label.push(<%request.getAttribute("label4");%>);
        	data.push(<%request.getAttribute("data5");%>);
        	label.push(<%request.getAttribute("label5");%>);
        	data.push(<%request.getAttribute("data6");%>);
        	label.push(<%request.getAttribute("label6");%>);
        	
            var ctx = document.getElementById('canvas').getContext('2d');
            window.myLine = new Chart(ctx, config);
            window.myLine.update();
            
            
        };

        

    </script>
    
    <form name="dataForm" action='DataServlet' method='post'>
		<button class="btn" type="submit" >Reload</button>
	</form>
</body>
</html>